-- MySQL dump 10.13  Distrib 8.0.29, for macos12 (x86_64)
--
-- Host: localhost    Database: logicea_card_services
-- ------------------------------------------------------
-- Server version	5.7.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cards`
--

DROP TABLE IF EXISTS `cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cards`
(
    `cardId`          int(10) unsigned NOT NULL AUTO_INCREMENT,
    `cardName`        varchar(55)  NOT NULL,
    `cardDescription` varchar(100) NOT NULL,
    `color`           varchar(40)           DEFAULT NULL,
    `status`          enum('TODO','IN PROGRESS','DONE') NOT NULL DEFAULT 'TODO',
    `dateCreated`     datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `dateModified`    datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `active`          int(11) NOT NULL DEFAULT '1',
    PRIMARY KEY (`cardId`),
    KEY               `idx_search_by_name` (`cardName`),
    KEY               `idx_search_by_color` (`color`),
    KEY               `idx_search_by_status` (`status`),
    KEY               `idx_search_by_dateCreated` (`dateCreated`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cards`
--

LOCK
TABLES `cards` WRITE;
/*!40000 ALTER TABLE `cards` DISABLE KEYS */;
INSERT INTO `cards`
VALUES (1, 'Test Card', '54775745747', 'blue', 'TODO', '2023-08-28 22:44:56', '2023-08-28 22:44:56', 1),
       (2, 'BLUE CARD', 'this is the first test card', '#0000FF', 'TODO', '2023-08-31 09:19:32', '2023-08-31 09:19:31',
        1),
       (3, 'BLUE CARD', 'this is the first test card', '#0000FF', 'TODO', '2023-08-31 09:20:30', '2023-08-31 09:20:30',
        1),
       (4, 'BLUE CARD', 'this is the first test card', '#0000FF', 'TODO', '2023-08-31 09:23:16', '2023-08-31 09:23:16',
        1),
       (6, 'BLUE CARD', 'this is the first test card', '#0000FF', 'TODO', '2023-08-31 09:32:40', '2023-08-31 09:32:39',
        1),
       (7, 'BLUE CARD', 'this is the first test card', '#0000FF', 'TODO', '2023-08-31 09:33:24', '2023-08-31 09:33:24',
        1),
       (8, 'BLUE CARD', 'this is the first test card', '#0000FF', 'TODO', '2023-08-31 09:33:32', '2023-08-31 09:33:32',
        1),
       (9, 'BLUE CARD', 'this is the first test card', '#0000FF', 'TODO', '2023-08-31 09:33:33', '2023-08-31 09:33:33',
        1),
       (10,
        'Kinuthia\'s Card','Another Test','','DONE','2023-08-31 09:34:06','2023-09-01 19:05:16',1),(11,'BLUE CARD','this is the first test card','#0000FF','TODO','2023-08-31 09:34:13','2023-08-31 09:34:12',1),(12,'BLUE CARD','this is the first test card',NULL,'TODO','2023-08-31 09:34:53','2023-08-31 09:34:53',1),(13,'BLUE CARD','this is the first test card','#0000FF','TODO','2023-08-31 09:35:20','2023-08-31 09:35:19',1),(14,'BLUE CARD','this is the first test card','#0000FF','TODO','2023-08-31 09:35:21','2023-08-31 09:35:21',1),(15,'BLUE CARD','this is the first test card','#0000FF','TODO','2023-08-31 09:35:22','2023-08-31 09:35:21',1),(16,'BLUE CARD','this is the first test card','#0000FF','TODO','2023-08-31 09:35:22','2023-08-31 09:35:22',1);
/*!40000 ALTER TABLE `cards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `groups` (
  `groupId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `groupName` varchar(55) NOT NULL,
  `groupDescription` varchar(100) NOT NULL,
  `dateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`groupId`),
  KEY `idx_search_by_group_name` (`groupName`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'Admin Group','Admin Group Capabilities','2023-08-28 22:37:57','2023-08-28 22:37:57',1),(2,'Member Group','Member Group Capabilities','2023-08-28 22:37:57','2023-08-28 22:37:57',1);
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permissions` (
  `permissionId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `groupId` int(10) unsigned NOT NULL,
  `permissionScope` enum('ADMIN','MEMBER') NOT NULL DEFAULT 'MEMBER',
  `permissionDescription` varchar(45) NOT NULL,
  `dateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`permissionId`),
  KEY `fk_group_id` (`groupId`),
  CONSTRAINT `fk_permissions_groups` FOREIGN KEY (`groupId`) REFERENCES `groups` (`groupId`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (4,1,'ADMIN','Admin capabilities','2023-08-30 21:45:13','2023-08-30 21:45:13',1),(5,2,'MEMBER','MEMBER CAPABILITIES','2023-08-30 21:46:22','2023-08-30 21:46:22',1);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userCards`
--

DROP TABLE IF EXISTS `userCards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `userCards` (
  `userCardId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL,
  `cardId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`userCardId`),
  UNIQUE KEY `idx_userid_card_id_unique` (`userId`,`cardId`),
  KEY `fk_user_cards_cards_idx` (`cardId`),
  KEY `idx_search_by_dateCreated` (`dateCreated`),
  CONSTRAINT `fk_user_cards_cards` FOREIGN KEY (`cardId`) REFERENCES `cards` (`cardId`) ON UPDATE CASCADE,
  CONSTRAINT `fk_user_cards_users` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userCards`
--

LOCK TABLES `userCards` WRITE;
/*!40000 ALTER TABLE `userCards` DISABLE KEYS */;
INSERT INTO `userCards` VALUES (1,1,1,'2023-08-28 22:45:21','2023-08-28 22:45:21',1),(2,2,3,'2023-08-31 09:20:30','2023-08-31 09:20:30',1),(3,2,4,'2023-08-31 09:23:16','2023-08-31 09:23:16',1),(5,2,6,'2023-08-31 09:32:40','2023-08-31 09:32:39',1),(6,2,7,'2023-08-31 09:33:24','2023-08-31 09:33:24',1),(7,2,8,'2023-08-31 09:33:32','2023-08-31 09:33:32',1),(8,2,9,'2023-08-31 09:33:33','2023-08-31 09:33:33',1),(9,2,10,'2023-08-31 09:34:06','2023-08-31 09:34:05',1),(10,2,11,'2023-08-31 09:34:13','2023-08-31 09:34:12',1),(11,2,12,'2023-08-31 09:34:53','2023-08-31 09:34:53',1),(12,2,13,'2023-08-31 09:35:20','2023-08-31 09:35:19',1),(13,2,14,'2023-08-31 09:35:21','2023-08-31 09:35:21',1),(14,2,15,'2023-08-31 09:35:22','2023-08-31 09:35:21',1),(15,2,16,'2023-08-31 09:35:22','2023-08-31 09:35:22',1);
/*!40000 ALTER TABLE `userCards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userGroups`
--

DROP TABLE IF EXISTS `userGroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `userGroups` (
  `userGroupId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL,
  `groupId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`userGroupId`),
  UNIQUE KEY `unique_userid_group_id` (`userId`,`groupId`),
  KEY `fk_user_groups_groups_idx` (`groupId`),
  CONSTRAINT `fk_user_groups_groups` FOREIGN KEY (`groupId`) REFERENCES `groups` (`groupId`) ON UPDATE CASCADE,
  CONSTRAINT `fk_user_groups_users` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userGroups`
--

LOCK TABLES `userGroups` WRITE;
/*!40000 ALTER TABLE `userGroups` DISABLE KEYS */;
INSERT INTO `userGroups` VALUES (1,1,1,'2023-08-28 22:44:21','2023-08-28 22:44:21',1),(2,2,2,'2023-08-28 22:44:21','2023-08-28 22:44:21',1);
/*!40000 ALTER TABLE `userGroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `userId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `dateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`userId`),
  KEY `idx_search_by_username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'john-doe-admin@logicea.com','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8','2023-08-28 22:43:12','2023-08-31 18:27:21',1),(2,'john.doe-normal@logicea.com','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8','2023-08-28 22:43:36','2023-08-31 18:27:21',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-01 19:33:24
