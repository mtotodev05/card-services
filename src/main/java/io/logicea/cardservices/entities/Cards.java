package io.logicea.cardservices.entities;

import com.google.gson.Gson;
import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

@Table(name = "cards")
@Setter
@Getter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Cards implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int cardId;
    @Column(name = "cardName")
    private String cardName;
    @Column(name = "cardDescription")
    private String cardDescription;
    @Column(name = "color")
    private String color;
    @Column(name = "status")
    private String status;
    @Column(name = "dateCreated")
    private Date dateCreated;

    public String toString() {
        return new Gson().toJson(this);
    }

}
