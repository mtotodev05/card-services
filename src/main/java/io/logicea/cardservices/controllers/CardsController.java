package io.logicea.cardservices.controllers;

import io.logicea.cardservices.dtos.request.CardRequest;
import io.logicea.cardservices.dtos.request.JWTAuthenticationRequest;
import io.logicea.cardservices.dtos.request.UpdateCardRequest;
import io.logicea.cardservices.dtos.response.JWTResponse;
import io.logicea.cardservices.dtos.response.ResponseArray;
import io.logicea.cardservices.service.AuthenticationService;
import io.logicea.cardservices.service.CardsService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("api/v1")
@Slf4j
@RequiredArgsConstructor
public class CardsController {
    public static final String FETCH_USER_CARDS = "fetch-user-cards";
    public static final String FETCH_ALL_CARDS = "fetch-allcards";
    public static final String CREATE_CARD = "create-card";
    public static final String SEARCH_CARD = "search-card";

    public static final String GET_CARD = "card/{cardId}";
    public static final String UPDATE_CARD = "update-card";
    public static final String DELETE_CARD = "delete-card/{cardId}";
    // Endpoints.
    private static final String GENERATE_JWT = "jwt";
    private final AuthenticationService authenticationService;
    private final CardsService cardsService;


    @PostMapping(GENERATE_JWT)
    public Mono<JWTResponse> onGenerateJwtResponse(@RequestBody @Valid JWTAuthenticationRequest
                                                           jwtAuthenticationRequest) {
        log.info("About to authenticate the request {}", jwtAuthenticationRequest);
        return this.authenticationService.onGenerateJWT(jwtAuthenticationRequest);
    }

    @PostMapping(CREATE_CARD)
    public Mono<ResponseArray> onCreateCards(@RequestBody @Valid CardRequest cardRequest) {
        return this.cardsService.onCreateCard(cardRequest);
    }

    @GetMapping(FETCH_USER_CARDS)
    public Mono<ResponseArray> onFetchUserCards() {
        return this.cardsService.onFetchUserCards();
    }

    @GetMapping(FETCH_ALL_CARDS)
    public Mono<ResponseArray> onFetchAllCards() {
        return this.cardsService.onFetchAllCards();
    }


    @GetMapping(SEARCH_CARD)
    public Mono<ResponseArray> searchCards(
            @RequestParam(name = "name", required = false) String name,
            @RequestParam(name = "color", required = false) String color,
            @RequestParam(name = "status", required = false) String status,
            @RequestParam(name = "dateCreated", required = false) String dateCreated,
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "size", defaultValue = "10") int size,
            @RequestParam(name = "sortBy", required = false) String sortBy
    ) {
        return this.cardsService.onSearchCards(name, color, status, dateCreated, page, size, sortBy);
    }

    @GetMapping(GET_CARD)
    public Mono<ResponseArray> getCard(@PathVariable(name = "cardId") int cardId) {
        return this.cardsService.onFetchSingleCard(cardId);
    }

    @DeleteMapping(DELETE_CARD)
    public Mono<ResponseArray> deleteCard(@PathVariable(name = "cardId") int cardId) {
        return this.cardsService.onDeleteCard(cardId);
    }

    @PatchMapping(UPDATE_CARD)
    public Mono<ResponseArray> onUpdateCard(@RequestBody @Valid UpdateCardRequest updateCardRequest) {
        return this.cardsService.onUpdateCard(updateCardRequest);
    }
}
