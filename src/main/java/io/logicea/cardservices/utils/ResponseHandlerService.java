package io.logicea.cardservices.utils;

import io.logicea.cardservices.configs.ApplicationProperties;
import io.logicea.cardservices.dtos.response.ResponseArray;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class ResponseHandlerService {
    private final ApplicationProperties applicationProperties;

    public ResponseArray onHandleSuccessResponse(List<Object> responseObject) {
        return ResponseArray.builder()
                .statusCode(String.valueOf(this.applicationProperties.getSuccessStatusCode()))
                .statusDescription(this.applicationProperties.getSuccessMessage())
                .data(responseObject)
                .build();
    }

    public ResponseArray onHandleNotFoundResponse() {
        return ResponseArray.builder()
                .statusCode(String.valueOf(this.applicationProperties.getNotFoundStatusCode()))
                .statusDescription(this.applicationProperties.getNotFoundStatusMessage())
                .data(null)
                .build();
    }
}
