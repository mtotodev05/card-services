package io.logicea.cardservices.validation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = UpdateNameValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidateUpdateName {

    String message() default "Update name if provided, should not be null";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
