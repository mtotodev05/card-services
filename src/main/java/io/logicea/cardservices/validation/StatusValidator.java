package io.logicea.cardservices.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class StatusValidator implements ConstraintValidator<ValidateStatus, String> {
    @Override
    public boolean isValid(String status, ConstraintValidatorContext constraintValidatorContext) {
        // If the status is null or empty, return true.
        if (status == null || status.isEmpty()) return true;
        return status.equalsIgnoreCase("TODO") || status.equalsIgnoreCase("IN PROGRESS") ||
                status.equalsIgnoreCase("DONE");
    }
}
