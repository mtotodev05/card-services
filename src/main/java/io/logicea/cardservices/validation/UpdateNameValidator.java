package io.logicea.cardservices.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class UpdateNameValidator implements ConstraintValidator<ValidateUpdateName, String> {
    @Override
    public boolean isValid(String updateName, ConstraintValidatorContext constraintValidatorContext) {
        if (updateName != null) {
            return !updateName.isEmpty();
        }
        return true;
    }
}
