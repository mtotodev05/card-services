package io.logicea.cardservices.validation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = StatusValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidateStatus {

    String message() default "Invalid status provided (TODO, IN PROGRESS, DONE)";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
