package io.logicea.cardservices.validation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ColorValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidateColor {
    String message() default "Invalid color format - Color, if provided, should conform to a “6 alphanumeric characters prefixed with a #“ format";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
