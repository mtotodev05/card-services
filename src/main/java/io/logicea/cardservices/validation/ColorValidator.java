package io.logicea.cardservices.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class ColorValidator implements ConstraintValidator<ValidateColor, String> {
    @Override
    public boolean isValid(String colorParameter, ConstraintValidatorContext constraintValidatorContext) {
        // If it's null or empty,means the user did not provide therefore return true;
        if (colorParameter == null || colorParameter.isEmpty()) return true;
        return colorParameter.matches("^#[0-9A-Fa-f]{6}$");
    }
}
