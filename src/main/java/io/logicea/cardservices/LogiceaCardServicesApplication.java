package io.logicea.cardservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LogiceaCardServicesApplication {

    public static void main(String[] args) {
        SpringApplication.run(LogiceaCardServicesApplication.class, args);
    }

}
