package io.logicea.cardservices.security.dtos;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serial;
import java.util.Collection;

@Getter
@Setter
public class JWTAuthenticationToken extends AbstractAuthenticationToken {

    @Serial
    private static final long serialVersionUID = 1L;
    private String sourceIPAddress;
    private String token;
    private String userName;
    private UserDetails principal;


    public JWTAuthenticationToken(
            String sourceIPAddress,
            String token
    ) {
        super(null);
        this.token = token;
        this.sourceIPAddress = sourceIPAddress;
        setAuthenticated(false);
    }

    public JWTAuthenticationToken(
            String sourceIPAddress,
            String token,
            UserDetails principal, Collection<? extends GrantedAuthority> authorities
    ) {

        super(authorities);
        this.principal = principal;
        this.token = token;
        this.sourceIPAddress = sourceIPAddress;
        super.setAuthenticated(true);
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return this.principal;
    }
}
