package io.logicea.cardservices.security.dtos.roles;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UserGroupDTO implements Serializable {
    private Long userGroupId;
    private GroupDTO groups;
    private Long dateCreated;
}
