package io.logicea.cardservices.security.dtos;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serial;

@Getter
@Setter
public class RoleAuthority implements GrantedAuthority {
    @Serial
    private static final long serialVersionUID = 1L;

    private String permissionScope;

    @Override
    public String getAuthority() {
        return permissionScope;
    }
}
