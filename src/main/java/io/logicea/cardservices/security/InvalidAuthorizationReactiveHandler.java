package io.logicea.cardservices.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.logicea.cardservices.dtos.response.JWTResponse;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.server.ServerAuthenticationEntryPoint;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

public class InvalidAuthorizationReactiveHandler implements ServerAuthenticationEntryPoint {
    private final ObjectMapper objectMapper;

    public InvalidAuthorizationReactiveHandler(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public Mono<Void> commence(ServerWebExchange exchange, AuthenticationException ex) {
        return Mono.defer(() -> {
            final JWTResponse responseJson = JWTResponse.builder()
                    .token(ex.getMessage())
                    .statusCode("403")
                    .build();

            ServerHttpResponse response = exchange.getResponse();
            response.setStatusCode(HttpStatus.FORBIDDEN);
            response.getHeaders().setContentType(MediaType.APPLICATION_JSON);
            final DataBufferFactory bufferFactory = response.bufferFactory();
            final Mono<DataBuffer> dataBufferMono = Mono.fromCallable(() -> objectMapper.writeValueAsBytes(responseJson))
                    .map(bufferFactory::wrap);
            return response.writeWith(dataBufferMono);
        });
    }
}
