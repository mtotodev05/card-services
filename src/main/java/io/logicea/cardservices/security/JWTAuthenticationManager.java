package io.logicea.cardservices.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.logicea.cardservices.configs.ApplicationProperties;
import io.logicea.cardservices.security.dtos.JWTAuthenticationToken;
import io.logicea.cardservices.security.dtos.RoleAuthority;
import io.logicea.cardservices.security.dtos.roles.UserDetailsDTO;
import io.logicea.cardservices.security.dtos.roles.UserGroupDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.core.Authentication;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class JWTAuthenticationManager implements ReactiveAuthenticationManager {
    private final Logger logger = LoggerFactory.getLogger(JWTAuthenticationManager.class);

    private final ApplicationProperties applicationProperties;
    private final ObjectMapper objectMapper;

    public JWTAuthenticationManager(ApplicationProperties applicationProperties, ObjectMapper objectMapper) {
        this.applicationProperties = applicationProperties;
        this.objectMapper = objectMapper;
    }

    @Override
    public Mono<Authentication> authenticate(Authentication authentication) {
        return Mono.fromCallable(() -> {
            logger.info("authenticating that the jwt provided is valid");
            final String jwt = ((JWTAuthenticationToken) authentication).getToken();
            try {
                Claims claims = Jwts.parser()
                        .setSigningKey(this.applicationProperties.getJwtSharedSecret().getBytes())
                        .parseClaimsJws(jwt).getBody();
                logger.info("ID: " + claims.getId());
                logger.info("Subject: " + claims.getSubject());
                logger.info("Issuer: " + claims.getIssuer());
                logger.info("Expiration: " + claims.getExpiration());
                logger.info("Roles " + new Gson().toJson(claims.get("groups")));
                // Get the roles.
                final UserGroupDTO[] rolesClaimsDTOList = new Gson().fromJson(new Gson().toJson(claims.get("groups")), UserGroupDTO[].class);
                //this.objectMapper.readValue(this.objectMapper.writeValueAsString(claims.get("groups")),GroupDTO.class);
                final List<List<RoleAuthority>> roleAuthorityList = Arrays.stream(rolesClaimsDTOList)
                        .map(groupDTO -> groupDTO.getGroups().getPermissions().stream()
                                .map(groupPermissionDTO -> {
                                    RoleAuthority roleAuthority = new RoleAuthority();
                                    roleAuthority.setPermissionScope(groupPermissionDTO.getPermissionScope());
                                    return roleAuthority;
                                })
                                .collect(Collectors.toList())).toList();


                UserDetailsDTO user = new UserDetailsDTO();
                user.setEmail(claims.get("email", String.class));
                user.setUserID(claims.get("userId", Integer.class));
                user.setUserName(claims.getSubject());
                user.setRoles(roleAuthorityList.get(0));

                return new JWTAuthenticationToken(((JWTAuthenticationToken) authentication).getSourceIPAddress(),
                        jwt, user, roleAuthorityList.get(0));
            } catch (Exception exception) {
                logger.error("Exception during JWT Validation {}", exception.getMessage());
                throw new AuthenticationServiceException(exception.getMessage());
            }
        });
    }
}
