package io.logicea.cardservices.security;

import io.logicea.cardservices.security.dtos.JWTAuthenticationToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.server.authentication.ServerAuthenticationConverter;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Optional;

public class JWTAuthenticationFilter implements ServerAuthenticationConverter {
    private final Logger logger = LoggerFactory.getLogger(JWTAuthenticationFilter.class);

    @Override
    public Mono<Authentication> convert(ServerWebExchange exchange) {
        // Get the request.
        final ServerHttpRequest serverHttpRequest = exchange.getRequest();
        final String extractedJwtString = this.validateJwtExists(serverHttpRequest);
        if (extractedJwtString == null) {
            logger.error("Could not validate that jwt exists");
            return Mono.empty();
        }
        final String sourceIPAddress = Optional.ofNullable(serverHttpRequest.getLocalAddress())
                .map(InetSocketAddress::getAddress)
                .map(InetAddress::getHostAddress)
                .orElse(null);
        final JWTAuthenticationToken jwtAuthenticationToken = new JWTAuthenticationToken(
                sourceIPAddress, extractedJwtString
        );
        return Mono.just(jwtAuthenticationToken);
    }

    private String validateJwtExists(ServerHttpRequest serverHttpRequest) {
        final String jwtHeader = serverHttpRequest.getHeaders().getFirst(HttpHeaders.AUTHORIZATION);
        logger.info("This is the jwt header {}", jwtHeader);
        if (jwtHeader == null) return null;
        if (!jwtHeader.toLowerCase().trim().startsWith("bearer ")) return null;
        final String[] jwtTokenArray = jwtHeader.trim().split(" ");
        if (jwtTokenArray.length != 2) return null;
        final String jwtToken = jwtTokenArray[1];
        return (jwtToken != null && !jwtToken.trim().isEmpty()) ? jwtToken : null;
    }
}
