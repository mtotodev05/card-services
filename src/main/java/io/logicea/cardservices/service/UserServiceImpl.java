package io.logicea.cardservices.service;

import io.logicea.cardservices.entities.Users;
import io.logicea.cardservices.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Override
    public Mono<Users> onFindUserByUsername(String username) {
        return Mono.defer(() -> Mono.defer(() -> Mono.fromCallable(() ->
                        this.userRepository.findByUsername(username))
                .flatMap(Mono::justOrEmpty)
                .subscribeOn(Schedulers.boundedElastic())));
    }

    @Override
    public Mono<Users> onAuthenticateUser(String username, String password) {
        log.info("We are authenticating the user provided {}", username);
        return this.onFindUserByUsername(username)
                .switchIfEmpty(Mono.error(new Exception("User Not Found Error")))
                .flatMap(users -> {
                    // Hash the password.
                    try {
                        var hashedPassword = this.hashingSHA256(password);
                        if (!hashedPassword.equalsIgnoreCase(users.getPassword())) {
                            return Mono.error(new Exception("Incorrect password provided"));
                        }
                    } catch (Exception e) {
                        log.info("An error ocurred while attempting to process password authentication");
                        return Mono.error(new Exception(e.getMessage()));
                    }
                    return Mono.just(users);
                });
    }

    @Override
    public Mono<Users> onIdentifyUserRequest() {
        return this.getReactiveCurrentUser()
                .switchIfEmpty(Mono.error(new Exception("Could not identify user in the request")))
                .flatMap(userDetailsDTO -> this.onFindUserByUsername(userDetailsDTO.getEmail()))
                .switchIfEmpty(Mono.error(new Exception("User with the provided username does not exist")));
    }
}
