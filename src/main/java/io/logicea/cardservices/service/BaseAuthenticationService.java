package io.logicea.cardservices.service;

import io.logicea.cardservices.security.dtos.roles.UserDetailsDTO;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import reactor.core.publisher.Mono;

public interface BaseAuthenticationService {
    default Mono<UserDetailsDTO> getReactiveCurrentUser() {
        return ReactiveSecurityContextHolder.getContext()
                .map(SecurityContext::getAuthentication)
                .map(Authentication::getPrincipal)
                .switchIfEmpty(Mono.empty())
                .cast(UserDetailsDTO.class);
    }
}
