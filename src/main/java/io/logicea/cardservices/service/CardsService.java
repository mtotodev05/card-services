package io.logicea.cardservices.service;

import io.logicea.cardservices.dtos.request.CardRequest;
import io.logicea.cardservices.dtos.request.UpdateCardRequest;
import io.logicea.cardservices.dtos.response.ResponseArray;
import reactor.core.publisher.Mono;

public interface CardsService extends BaseAuthenticationService {

    Mono<ResponseArray> onCreateCard(CardRequest cardRequest);

    Mono<ResponseArray> onFetchUserCards();

    Mono<ResponseArray> onFetchAllCards();

    Mono<ResponseArray> onSearchCards(String cardName, String color,
                                      String status, String dateCreated, int page, int size, String sortBy);

    Mono<ResponseArray> onUpdateCard(UpdateCardRequest updateCardRequest);

    Mono<ResponseArray> onDeleteCard(int cardId);

    Mono<ResponseArray> onFetchSingleCard(int cardId);
}
