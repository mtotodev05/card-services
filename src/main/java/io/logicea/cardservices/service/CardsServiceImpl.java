package io.logicea.cardservices.service;

import io.logicea.cardservices.configs.ApplicationProperties;
import io.logicea.cardservices.dtos.request.CardRequest;
import io.logicea.cardservices.dtos.request.UpdateCardRequest;
import io.logicea.cardservices.dtos.response.ResponseArray;
import io.logicea.cardservices.entities.Cards;
import io.logicea.cardservices.entities.UserCards;
import io.logicea.cardservices.repository.CardsRepository;
import io.logicea.cardservices.repository.UserCardsRepository;
import io.logicea.cardservices.repository.spec.UserCardSpecification;
import io.logicea.cardservices.utils.ResponseHandlerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class CardsServiceImpl implements CardsService {
    private final UserService userService;
    private final CardsRepository cardsRepository;
    private final UserCardsRepository userCardsRepository;
    private final ResponseHandlerService responseHandlerService;
    private final ApplicationProperties applicationProperties;

    @Override
    public Mono<ResponseArray> onCreateCard(CardRequest cardRequest) {
        //First let's get the user identity.
        return this.userService.onIdentifyUserRequest()
                .flatMap(users -> {
                    // Let's construct the cards object.
                    var cardObject = Cards.builder()
                            .cardName(cardRequest.getCardName())
                            .color(cardRequest.getColor())
                            .cardDescription(cardRequest.getDescription())
                            .dateCreated(new Date())
                            .status(this.applicationProperties.getDefaultCardStatus())
                            .build();
                    return Mono.fromCallable(() -> {
                        var cardsObject = this.cardsRepository
                                .save(cardObject);
                        // Let's create the user cards.
                        log.info("Persisted the cards object {}", cardsObject);
                        var userCards = UserCards.builder()
                                .user(users)
                                .cards(cardsObject)
                                .dateCreated(new Date())
                                .build();
                        var userCardsObject = this.userCardsRepository.save(userCards);
                        log.info("Persisted the user cards object {}", userCardsObject);
                        return this.responseHandlerService.onHandleSuccessResponse(List.of(userCards));
                    }).subscribeOn(Schedulers.boundedElastic());
                });
    }

    @Override
    public Mono<ResponseArray> onFetchUserCards() {
        //First let's get the user identity.
        return this.userService.onIdentifyUserRequest()
                .flatMap(users -> Mono.fromCallable(() -> {
                    var userCardsList = this.userCardsRepository.findAllByUser(users);
                    var cardList = userCardsList.stream().map(UserCards::getCards).toList();
                    return this.responseHandlerService
                            .onHandleSuccessResponse(Collections.singletonList(cardList));
                }).subscribeOn(Schedulers.boundedElastic()));
    }

    @Override
    public Mono<ResponseArray> onFetchAllCards() {
        return Mono.fromCallable(() -> {
            var userCardsList = this.userCardsRepository.findAll();
            var cardList = userCardsList.stream().map(UserCards::getCards).toList();
            return this.responseHandlerService
                    .onHandleSuccessResponse(Collections.singletonList(cardList));
        }).subscribeOn(Schedulers.boundedElastic());
    }

    @Override
    public Mono<ResponseArray> onSearchCards(String cardName, String color, String status, String dateCreated,
                                             int page, int size, String sortBy) {
        Pageable pageable;
        if (sortBy != null && !sortBy.isEmpty()) {
            pageable = PageRequest.of(page, size, Sort.by(sortBy));
        } else {
            pageable = PageRequest.of(page, size);
        }
        return this.userService.onIdentifyUserRequest().flatMap(userDetailsDTO -> Mono.fromCallable(() -> {

            final Specification<UserCards> userCardsSpecification = Specification
                    .where(UserCardSpecification.user(userDetailsDTO.getUsername()))
                    .and(UserCardSpecification.cardName(cardName))
                    .and(UserCardSpecification.cardStatus(status))
                    .and(UserCardSpecification.cardColor(color))
                    .and(UserCardSpecification.dateCreated(dateCreated));

            List<UserCards> userCards = this.userCardsRepository.findAll(userCardsSpecification, pageable)
                    .getContent();
            var cardList = userCards.stream().map(UserCards::getCards).toList();
            return this.responseHandlerService
                    .onHandleSuccessResponse(Collections.singletonList(cardList));
        }).subscribeOn(Schedulers.boundedElastic()));

    }

    @Override
    public Mono<ResponseArray> onUpdateCard(UpdateCardRequest updateCardRequest) {
        return this.userService.onIdentifyUserRequest().flatMap(users -> {
            // Let's get the user card where card id has been provided.
            var foundCardList = users.getUserCards().stream()
                    .map(UserCards::getCards)
                    .filter(cards -> cards.getCardId() == Integer.parseInt(updateCardRequest.getCardId())).toList();
            if (foundCardList.isEmpty()) {
                // we return an empty list.
                return Mono.just(this.responseHandlerService.onHandleNotFoundResponse());
            }
            return Mono.fromCallable(() -> {
                var updateCard = foundCardList.get(0);
                if (updateCardRequest.getName() != null && !updateCardRequest.getName().isEmpty())
                    updateCard.setCardName(updateCardRequest.getName());
                updateCard.setCardDescription(updateCardRequest.getDescription());
                updateCard.setStatus(updateCardRequest.getStatus());
                updateCard.setColor(updateCardRequest.getColor());
                var userCardsObject = this.cardsRepository.save(updateCard);
                return this.responseHandlerService.onHandleSuccessResponse(List.of(userCardsObject));
            }).subscribeOn(Schedulers.boundedElastic());
        });
    }

    @Override
    public Mono<ResponseArray> onDeleteCard(int cardId) {
        return this.userService.onIdentifyUserRequest()
                .flatMap(users -> {
                    // Let's get the user card where card id has been provided.
                    var userCardFound = users.getUserCards().stream().filter(userCards ->
                            userCards.getUser().getUsername().equalsIgnoreCase(users.getUsername())
                                    && userCards.getCards().getCardId() == cardId).toList();
                    if (userCardFound.isEmpty()) {
                        // we return an empty list.
                        return Mono.just(this.responseHandlerService.onHandleNotFoundResponse());
                    }
                    return Mono.fromCallable(() -> {
                        // First we delete the usercards.
                        userCardsRepository.delete(userCardFound.get(0));
                        cardsRepository.delete(userCardFound.get(0).getCards());
                        return responseHandlerService.onHandleSuccessResponse(null);
                    }).subscribeOn(Schedulers.boundedElastic());
                });
    }

    @Override
    public Mono<ResponseArray> onFetchSingleCard(int cardId) {
        return this.userService.onIdentifyUserRequest().flatMap(users -> {
            // Let's get the user card where card id has been provided.
            var foundCardList = users.getUserCards().stream()
                    .map(UserCards::getCards)
                    .filter(cards -> cards.getCardId() == cardId).toList();
            if (foundCardList.isEmpty()) {
                // we return an empty list.
                return Mono.just(this.responseHandlerService.onHandleNotFoundResponse());
            }
            return Mono.just(responseHandlerService.onHandleSuccessResponse(Collections.singletonList(foundCardList)));
        });
    }
}
