package io.logicea.cardservices.dtos.request;

import com.google.gson.Gson;
import io.logicea.cardservices.validation.ValidateColor;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CardRequest implements Serializable {

    @NotNull(message = "cardName is required")
    private String cardName;

    private String description;
    @ValidateColor
    private String color;

    public String toString() {
        return new Gson().toJson(this);
    }
}
