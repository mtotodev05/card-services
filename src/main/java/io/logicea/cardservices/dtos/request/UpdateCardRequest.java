package io.logicea.cardservices.dtos.request;

import io.logicea.cardservices.validation.ValidateColor;
import io.logicea.cardservices.validation.ValidateStatus;
import io.logicea.cardservices.validation.ValidateUpdateName;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateCardRequest implements Serializable {
    @NotEmpty(message = "card id is required")
    private String cardId;
    @ValidateUpdateName
    private String name;
    private String description;
    @ValidateColor
    private String color;
    @ValidateStatus
    private String status;
}
