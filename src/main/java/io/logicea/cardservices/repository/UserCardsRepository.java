package io.logicea.cardservices.repository;

import io.logicea.cardservices.entities.UserCards;
import io.logicea.cardservices.entities.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserCardsRepository extends JpaRepository<UserCards, Integer>,
        PagingAndSortingRepository<UserCards, Integer>,
        QueryByExampleExecutor<UserCards>, JpaSpecificationExecutor<UserCards> {

    List<UserCards> findAllByUser(Users user);

}
