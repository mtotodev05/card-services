package io.logicea.cardservices.repository.spec;

import io.logicea.cardservices.entities.Cards;
import io.logicea.cardservices.entities.UserCards;
import io.logicea.cardservices.entities.Users;
import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;

import java.text.SimpleDateFormat;
import java.util.Date;


public class UserCardSpecification {
    private static final Logger logger = LoggerFactory.getLogger(UserCardSpecification.class);


    // Let's connect to the users object - to give users only cards they have access to.
    public static Specification<UserCards> user(final String username) {
        return ((root, query, criteriaBuilder) -> {
            final Join<UserCards, Users> userCardsUsersJoin = root.join("user");
            final Predicate predicate = criteriaBuilder.equal(userCardsUsersJoin.get("username"), username);
            query.distinct(true);
            return predicate;
        });
    }

    // Card Name
    public static Specification<UserCards> cardName(final String cardName) {
        return ((root, query, criteriaBuilder) -> {
            if (cardName == null || cardName.isEmpty()) {
                return criteriaBuilder.and();
            }
            final Join<UserCards, Cards> userCardsCardsListJoin = root.join("cards");
            final Expression<String> lowerCardName = criteriaBuilder.lower(userCardsCardsListJoin.get("cardName"));
            Predicate predicate = criteriaBuilder.like(lowerCardName, criteriaBuilder.lower(criteriaBuilder.literal("%" + cardName + "%")));
            query.distinct(true);
            return predicate;
        });
    }

    // Status.
    public static Specification<UserCards> cardStatus(final String status) {
        return ((root, query, criteriaBuilder) -> {
            if (status == null || status.isEmpty()) {
                return criteriaBuilder.and();
            }
            final Join<UserCards, Cards> userCardsCardsListJoin = root.join("cards");
            final Expression<String> lowerCardName = criteriaBuilder.lower(userCardsCardsListJoin.get("status"));
            Predicate predicate = criteriaBuilder.like(lowerCardName, criteriaBuilder
                    .lower(criteriaBuilder.literal("%" + status + "%")));
            query.distinct(true);
            return predicate;
        });
    }

    // Color.
    public static Specification<UserCards> cardColor(final String color) {
        return ((root, query, criteriaBuilder) -> {
            if (color == null || color.isEmpty()) {
                return criteriaBuilder.and();
            }
            final Join<UserCards, Cards> userCardsCardsListJoin = root.join("cards");
            final Expression<String> lowerCardName = criteriaBuilder.lower(userCardsCardsListJoin.get("color"));
            Predicate predicate = criteriaBuilder.like(lowerCardName, criteriaBuilder
                    .lower(criteriaBuilder.literal("%" + color + "%")));
            query.distinct(true);
            return predicate;
        });
    }

    // Date Created.
    public static Specification<UserCards> dateCreated(final String dateCreated) {
        return (root, query, criteriaBuilder) -> {
            try {
                if (dateCreated == null || dateCreated.isEmpty()) {
                    return criteriaBuilder.and();
                }
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date dateCreatedTransformed = simpleDateFormat.parse(dateCreated);
                return criteriaBuilder.equal(criteriaBuilder.function("date", Date.class, root.get("dateCreated")), dateCreatedTransformed);
            } catch (Exception exception) {
                logger.error("An Error Occurred while trying to compare date {}", exception.getMessage());
                return criteriaBuilder.and();
            }
        };
    }
}
