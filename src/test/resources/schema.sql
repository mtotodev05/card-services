CREATE TABLE cards
(
    cardId          INT AUTO_INCREMENT PRIMARY KEY,
    cardName        VARCHAR(55)                        NOT NULL,
    cardDescription VARCHAR(100)                       NOT NULL,
    color           VARCHAR(40),
    status          ENUM('TODO','IN PROGRESS','DONE') DEFAULT 'TODO' NOT NULL,
    dateCreated     DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dateModified    DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL,
    active          INT      DEFAULT '1'               NOT NULL
);
ALTER TABLE cards
    ALTER COLUMN cardId RESTART WITH 17;

CREATE TABLE groups
(
    groupId          INT AUTO_INCREMENT PRIMARY KEY,
    groupName        VARCHAR(55)                        NOT NULL,
    groupDescription VARCHAR(100)                       NOT NULL,
    dateCreated      DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dateModified     DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL,
    active           INT      DEFAULT '1'               NOT NULL
);

-- H2-specific syntax to update the AUTO_INCREMENT value
ALTER TABLE groups
    ALTER COLUMN groupId RESTART WITH 3;

CREATE TABLE permissions
(
    permissionId          INT AUTO_INCREMENT PRIMARY KEY,
    groupId               INT                                NOT NULL,
    permissionScope       VARCHAR(45)                        NOT NULL DEFAULT 'MEMBER',
    permissionDescription VARCHAR(45)                        NOT NULL,
    dateCreated           DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    dateModified          DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL,
    active                INT      DEFAULT '1'               NOT NULL,
    FOREIGN KEY (groupId) REFERENCES groups (groupId) ON UPDATE CASCADE
);

-- H2-specific syntax to update the AUTO_INCREMENT value
ALTER TABLE permissions
    ALTER COLUMN permissionId RESTART WITH 6;

CREATE TABLE users
(
    userId       INT AUTO_INCREMENT PRIMARY KEY,
    username     VARCHAR(100) NOT NULL,
    password     VARCHAR(100) NOT NULL,
    dateCreated  DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    dateModified DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    active       INT                   DEFAULT '1' NOT NULL,
    UNIQUE (username)
);

-- H2-specific syntax to update the AUTO_INCREMENT value
ALTER TABLE users
    ALTER COLUMN userId RESTART WITH 3;

CREATE TABLE userCards
(
    userCardId   INT AUTO_INCREMENT PRIMARY KEY,
    userId       INT      NOT NULL,
    cardId       INT      NOT NULL,
    dateCreated  DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    dateModified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    active       INT               DEFAULT '1' NOT NULL,
    UNIQUE (userId, cardId),
    FOREIGN KEY (cardId) REFERENCES cards (cardId) ON UPDATE CASCADE,
    FOREIGN KEY (userId) REFERENCES users (userId) ON UPDATE CASCADE
);

-- H2-specific syntax to update the AUTO_INCREMENT value
ALTER TABLE userCards
    ALTER COLUMN userCardId RESTART WITH 16;

CREATE TABLE userGroups
(
    userGroupId  INT AUTO_INCREMENT PRIMARY KEY,
    userId       INT      NOT NULL,
    groupId      INT      NOT NULL,
    dateCreated  DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    dateModified DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    active       INT               DEFAULT '1' NOT NULL,
    UNIQUE (userId, groupId),
    FOREIGN KEY (groupId) REFERENCES groups (groupId) ON UPDATE CASCADE,
    FOREIGN KEY (userId) REFERENCES users (userId) ON UPDATE CASCADE
);

-- H2-specific syntax to update the AUTO_INCREMENT value
ALTER TABLE userGroups
    ALTER COLUMN userGroupId RESTART WITH 3;

INSERT INTO cards (cardId, cardName, cardDescription, color, status, dateCreated, dateModified, active)
VALUES (1, 'Test Card', '54775745747', 'blue', 'TODO', '2023-08-28 22:44:56', '2023-08-28 22:44:56', 1),
       (2, 'BLUE CARD', 'this is the first test card', '#0000FF', 'TODO', '2023-08-31 09:19:32', '2023-08-31 09:19:31',
        1),
       (3, 'BLUE CARD', 'this is the first test card', '#0000FF', 'TODO', '2023-08-31 09:20:30', '2023-08-31 09:20:30',
        1),
       (4, 'BLUE CARD', 'this is the first test card', '#0000FF', 'TODO', '2023-08-31 09:23:16', '2023-08-31 09:23:16',
        1),
       (6, 'BLUE CARD', 'this is the first test card', '#0000FF', 'TODO', '2023-08-31 09:32:40', '2023-08-31 09:32:39',
        1),
       (7, 'BLUE CARD', 'this is the first test card', '#0000FF', 'TODO', '2023-08-31 09:33:24', '2023-08-31 09:33:24',
        1),
       (8, 'BLUE CARD', 'this is the first test card', '#0000FF', 'TODO', '2023-08-31 09:33:32', '2023-08-31 09:33:32',
        1),
       (9, 'BLUE CARD', 'this is the first test card', '#0000FF', 'TODO', '2023-08-31 09:33:33', '2023-08-31 09:33:33',
        1),
       (10, 'Kinuthia''s Card', 'Another Test', '', 'DONE', '2023-08-31 09:34:06', '2023-09-01 19:05:16', 1),
       (11, 'BLUE CARD', 'this is the first test card', '#0000FF', 'TODO', '2023-08-31 09:34:13', '2023-08-31 09:34:12',
        1),
       (12, 'BLUE CARD', 'this is the first test card', NULL, 'TODO', '2023-08-31 09:34:53', '2023-08-31 09:34:53', 1),
       (13, 'BLUE CARD', 'this is the first test card', '#0000FF', 'TODO', '2023-08-31 09:35:20', '2023-08-31 09:35:19',
        1),
       (14, 'BLUE CARD', 'this is the first test card', '#0000FF', 'TODO', '2023-08-31 09:35:21', '2023-08-31 09:35:21',
        1),
       (15, 'BLUE CARD', 'this is the first test card', '#0000FF', 'TODO', '2023-08-31 09:35:22', '2023-08-31 09:35:21',
        1),
       (16, 'BLUE CARD', 'this is the first test card', '#0000FF', 'TODO', '2023-08-31 09:35:22', '2023-08-31 09:35:22',
        1);

INSERT INTO groups (groupId, groupName, groupDescription, dateCreated, dateModified, active)
VALUES (1, 'Admin Group', 'Admin Group Capabilities', '2023-08-28 22:37:57', '2023-08-28 22:37:57', 1),
       (2, 'Member Group', 'Member Group Capabilities', '2023-08-28 22:37:57', '2023-08-28 22:37:57', 1);


INSERT INTO permissions (permissionId, groupId, permissionScope, permissionDescription, dateCreated, dateModified,
                         active)
VALUES (4, 1, 'ADMIN', 'Admin capabilities', '2023-08-30 21:45:13', '2023-08-30 21:45:13', 1),
       (5, 2, 'MEMBER', 'MEMBER CAPABILITIES', '2023-08-30 21:46:22', '2023-08-30 21:46:22', 1);

INSERT INTO users (userId, username, password, dateCreated, dateModified, active)
VALUES (1, 'john-doe-admin@logicea.com', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8',
        '2023-08-28 22:43:12', '2023-08-31 18:27:21', 1),
       (2, 'john.doe-normal@logicea.com', '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8',
        '2023-08-28 22:43:36', '2023-08-31 18:27:21', 1);

INSERT INTO userCards (userCardId, userId, cardId, dateCreated, dateModified, active)
VALUES (1, 1, 1, '2023-08-28 22:45:21', '2023-08-28 22:45:21', 1),
       (2, 2, 3, '2023-08-31 09:20:30', '2023-08-31 09:20:30', 1),
       (3, 2, 4, '2023-08-31 09:23:16', '2023-08-31 09:23:16', 1),
       (5, 2, 6, '2023-08-31 09:32:40', '2023-08-31 09:32:39', 1),
       (6, 2, 7, '2023-08-31 09:33:24', '2023-08-31 09:33:24', 1),
       (7, 2, 8, '2023-08-31 09:33:32', '2023-08-31 09:33:32', 1),
       (8, 2, 9, '2023-08-31 09:33:33', '2023-08-31 09:33:33', 1),
       (9, 2, 10, '2023-08-31 09:34:06', '2023-08-31 09:34:05', 1),
       (10, 2, 11, '2023-08-31 09:34:13', '2023-08-31 09:34:12', 1),
       (11, 2, 12, '2023-08-31 09:34:53', '2023-08-31 09:34:53', 1),
       (12, 2, 13, '2023-08-31 09:35:20', '2023-08-31 09:35:19', 1),
       (13, 2, 14, '2023-08-31 09:35:21', '2023-08-31 09:35:21', 1),
       (14, 2, 15, '2023-08-31 09:35:22', '2023-08-31 09:35:21', 1),
       (15, 2, 16, '2023-08-31 09:35:22', '2023-08-31 09:35:22', 1);


INSERT INTO userGroups (userGroupId, userId, groupId, dateCreated, dateModified, active)
VALUES (1, 1, 1, '2023-08-28 22:44:21', '2023-08-28 22:44:21', 1),
       (2, 2, 2, '2023-08-28 22:44:21', '2023-08-28 22:44:21', 1);


