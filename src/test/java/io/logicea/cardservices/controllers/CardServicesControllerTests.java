package io.logicea.cardservices.controllers;

import io.logicea.cardservices.dtos.request.JWTAuthenticationRequest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

@AutoConfigureWebTestClient(timeout = "360000")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CardServicesControllerTests {

    public static final String USER_JWT = "eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTM1OTAwMjgsInN1YiI6ImpvaG4uZG9lLW5vcm1hbEBsb2dpY2VhLmNvbSIsInVzZXJJZCI6MiwiZW1haWwiOiJqb2huLmRvZS1ub3JtYWxAbG9naWNlYS5jb20iLCJncm91cHMiOlt7InVzZXJHcm91cElkIjoyLCJncm91cHMiOnsiZ3JvdXBJZCI6MiwiZ3JvdXBOYW1lIjoiTWVtYmVyIEdyb3VwIiwiZ3JvdXBEZXNjcmlwdGlvbiI6Ik1lbWJlciBHcm91cCBDYXBhYmlsaXRpZXMiLCJkYXRlQ3JlYXRlZCI6MTY5MzI1MTQ3NzAwMCwicGVybWlzc2lvbnMiOlt7InBlcm1pc3Npb25JZCI6NSwicGVybWlzc2lvblNjb3BlIjoiTUVNQkVSIiwicGVybWlzc2lvbkRlc2NyaXB0aW9uIjoiTUVNQkVSIENBUEFCSUxJVElFUyIsImRhdGVDcmVhdGVkIjoxNjkzNDIxMTgyMDAwfV19LCJkYXRlQ3JlYXRlZCI6MTY5MzI1MTg2MTAwMH1dLCJleHAiOjMxNjkzNTkwMDI4fQ.swCiL_wBYmNtu0ImIn95pcfjks-t6yhSAokGOradg68";
    public static final String INVALID_JWT = "JhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTM1OTAyMzAsInN1YiI6ImpvaG4tZG9lLWFkbWluQGxvZ2ljZWEuY29tIiwidXNlcklkIjoxLCJlbWFpbCI6ImpvaG4tZG9lLWFkbWluQGxvZ2ljZWEuY29tIiwiZ3JvdXBzIjpbeyJ1c2VyR3JvdXBJZCI6MSwiZ3JvdXBzIjp7Imdyb3VwSWQiOjEsImdyb3VwTmFtZSI6IkFkbWluIEdyb3VwIiwiZ3JvdXBEZXNjcmlwdGlvbiI6IkFkbWluIEdyb3VwIENhcGFiaWxpdGllcyIsImRhdGVDcmVhdGVkIjoxNjkzMjUxNDc3MDAwLCJwZXJtaXNzaW9ucyI6W3sicGVybWlzc2lvbklkIjo0LCJwZXJtaXNzaW9uU2NvcGUiOiJBRE1JTiIsInBlcm1pc3Npb25EZXNjcmlwdGlvbiI6IkFkbWluIGNhcGFiaWxpdGllcyIsImRhdGVDcmVhdGVkIjoxNjkzNDIxMTEzMDAwfV19LCJkYXRlQ3JlYXRlZCI6MTY5MzI1MTg2MTAwMH1dLCJleHAiOjMxNjkzNTkwMjMwfQ.LQOAQ83CrKqbQFyL4CUmvlAQ6CelGmMfkVtjBFGJnFk";
    private static final String GENERATE_JWT = "/api/v1/jwt";
    private static final String FETCH_USER_CARDS = "/api/v1/fetch-user-cards";
    private static final String FETCH_ALL_CARDS = "/api/v1/fetch-allcards";
    @Autowired
    private WebTestClient webClient;


    @Test
    @DisplayName("Test that given correct username and password, jwt is generated")
    public void testThatGivenCorrectCredentialsJwtIsGenerated() {
        // Construct the username and password
        var jwtRequest = JWTAuthenticationRequest
                .builder()
                .username("john-doe-admin@logicea.com")
                .password("password")
                .build();

        this.webClient.post()
                .uri(GENERATE_JWT)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(jwtRequest))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo("200");
    }

    @Test
    @DisplayName("Test that given incorrect username and password, jwt is not generated")
    public void testThatGivenInCorrectCredentialsJwtIsNotGenerated() {
        // Construct the username and password
        var jwtRequest = JWTAuthenticationRequest
                .builder()
                .username("john-doe-admin@logicea3.com")
                .password("password")
                .build();

        this.webClient.post()
                .uri(GENERATE_JWT)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(jwtRequest))
                .exchange()
                .expectStatus().is5xxServerError()
                .expectBody()
                .jsonPath("$.errorCode").isEqualTo("User Not Found Error");
    }

    // Fetch User cards.
    @Test
    @DisplayName("Test that an authenticated user is able to fetch their cards")
    public void testThatAuthenticatedUsersCanFetchTheirCards() {
        this.webClient.get()
                .uri(FETCH_USER_CARDS)
                .header("Authorization", "Bearer " + USER_JWT)
                .exchange()
                .expectStatus().is2xxSuccessful()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo("200")
                .jsonPath("$.statusDescription").isEqualTo("Operation Completed Successfully")
                .jsonPath("$.data[0][0].cardName").isEqualTo("BLUE CARD");
    }

    @Test
    @DisplayName("Test that a normal authenticated user cannot access all cards")
    public void testThatAuthenticatedNormalUsersCannotAccessAllCards() {
        this.webClient.get()
                .uri(FETCH_ALL_CARDS)
                .header("Authorization", "Bearer " + USER_JWT)
                .exchange()
                .expectStatus().is5xxServerError()
                .expectBody()
                .jsonPath("$.errorCode").isEqualTo("Access Denied");
    }

    @Test
    @DisplayName("Test that an non-authenticated user is not able to fetch their cards")
    public void testThatNonAuthenticatedUsersCannotFetchTheirCards() {
        this.webClient.get()
                .uri(FETCH_USER_CARDS)
                .exchange()
                .expectStatus().is4xxClientError()
                .expectBody()
                .jsonPath("$.statusCode").isEqualTo("403");
    }

    @Test
    @DisplayName("Test that an non-authenticated user is not able to fetch their cards - Broken Jwt")
    public void testThatNonAuthenticatedUsersCannotFetchTheirCardsBrokenJwt() {
        this.webClient.get()
                .uri(FETCH_USER_CARDS)
                .header("Authorization", "Bearer " + INVALID_JWT)
                .exchange()
                .expectStatus().is5xxServerError()
                .expectBody();
    }

}
