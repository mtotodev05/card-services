## Card Services

Click [here](https://app.getguru.com/card/iyBBynLT/10-Logicea-Cards-Applications) to view detailed implementation plan
of this service

### How to the run the application ?

### 1. Running the application locally.

Follow the following steps to run the project locally.

##### 1.1 Clone the project.

```
git clone https://github.com/kamaubrian/logicea-web-services.git
```

##### 1.2 Setup your database.

```
The database source script is located under /src/main/resources/card-services.sql
```

##### 1.3 Update your configurations to match your db credentials.

```
spring.datasource.url=jdbc:mysql://localhost:3306/<your-db-name>?useSSL=false&allowPublicKeyRetrieval=true
spring.datasource.username=<your-db-username>
spring.datasource.password=<your-db-password>
```

##### 1.4 Run the service and test.

By Default, this service runs on 8080. You can use the following
postman [collection](https://api.postman.com/collections/2090765-044fa374-4723-43cf-ba68-ec748b5dbe53?access_key=PMAT-01H98TW34WSF6RRMVEKAWX7F52)
to test.

##### 1.5 HTTP Contracts Implemented.

| NAME                          | ENDPOINT                                                 | Method   | Request Body                                                                                                  
|-------------------------------|----------------------------------------------------------|----------|---------------------------------------------------------------------------------------------------------------
| 1.Generate User JWT           | `/api/v1/jwt`                                            | `POST`   | ``` {"username": "john.doe-normal@logicea.com","password": "password"} ```                                    
| 2.Create Cards                | `/api/v1/create-card`                                    | `POST`   | ```{"cardName": "BLUE CARD","description": "this is the first test card","color":"#0000FF"} ```               
| 3. Update Cards               | `/api/v1/update-card`                                    | `PATCH`  | ``` {"cardId": "10","name": "Kinuthia's Card","description": "Another Test","color": "","status": "DONE"} ``` 
| 4. Fetch User Cards           | `/api/v1/fetch-user-cards`                               | `GET`    | `N/A`                                                                                                         
| 5. Access All Cards - (ADMIN) | `/api/v1/fetch-allcards`                                 | `GET`    | `N/A`                                                                                                         
| 6. Search Card                | `/api/v1/search-card?status=a&name=2&sortBy=dateCreated` | `GET`    | `N/A`                                                                                                         
| 7. Get Single Card            | `/api/v1/card/{cardId}`                                  | `GET`    | `N/A`                                                                                                         
| 8. Delete Card                | `/api/v1/delete-card/{cardId}`                           | `DELETE` | `N/A`                                                                                                         

